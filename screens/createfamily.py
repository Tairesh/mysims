import pygame
import colors
import gui
import game
import images
from translate import _
from gamescreen import GameScreen


class CreateFamily(GameScreen):
    def __init__(self):
        super().__init__()
        self.reset()
        self.sims = []

        buttonWidth = 120
        buttonHeight = 40
        btnBack = gui.Button(pygame.Rect(self.screenWidth-buttonWidth-16, 5, buttonWidth, buttonHeight), _('Back'),
                         lambda self: game.changeScreen('WorldPreview', {'world': self.config['world']}), icon=images.BUTTON_BACK, key=pygame.K_ESCAPE)
        self.buttons.append(btnBack)

        iconWidth = 64
        iconHeight = 64
        iconOffset = 10
        x = 10
        for sim in self.sims:
            pass
        btnAdd = gui.Button(pygame.Rect(x, self.screenHeight-iconHeight-iconOffset, iconWidth, iconHeight), '+',
                    lambda screen: game.changeScreen('CreateSim', {'world': self.config['world']}, True), key=pygame.K_PLUS)
        self.buttons.append(btnAdd)

    def reset(self):
        super().reset()
        self.sims = []

    def clear(self):
        super().clear()
        game.surface.fill(colors.LIGHT_BLUE)

    def call(self, event):
        super().call(event)

    def draw(self):
        super().draw()
