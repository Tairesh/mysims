import pygame
import colors
import gui
import game
import images
from translate import _
from gamescreen import GameScreen


class Sim:
    def __init__(self):
        self.firstName = ''
        self.lastName = ''
        self.age = 25
        self.gender = False

    def update(self, attr, value):
        self.__setattr__(attr, value)
        if attr in ('age', 'gender'):
            game.currentScreen.redraw()
        # print(self.firstName, self.lastName, self.age, self.gender, sep=' ')


class CreateSim(GameScreen):
    def __init__(self):
        super().__init__()
        self.sim = Sim()
        self.reset()

        buttonWidth = 120
        buttonHeight = 40

        self.textFirstName = game.font.render(_("First name:"), False, colors.WHITE)
        self.textLastName = game.font.render(_("Last name:"), False, colors.WHITE)
        self.inputFirstName = gui.TextInput(self.textFirstName.get_width() + 15, 10, 200, 30,
                                           text=self.sim.firstName, key='firstName', onChange=lambda value: game.currentScreen.sim.update('firstName', value))
        self.inputLastName = gui.TextInput(self.inputFirstName.rect.right + 20 + self.textLastName.get_width(), 10, 200, 30,
                                           text=self.sim.lastName, key='lastName', onChange=lambda value: game.currentScreen.sim.update('lastName', value))
        self.inputs.append(self.inputFirstName)
        self.inputs.append(self.inputLastName)

        btnGenderMale = gui.Button(pygame.Rect(10, 45, 70, 40), _('Male'), click=lambda screen: screen.sim.update('gender', True),
                                   key='genderMale', radio='gender', state=gui.STATE_ACTIVE if self.sim.gender else gui.STATE_DEFAULT)
        btnGenderFemale = gui.Button(pygame.Rect(85, 45, 70, 40), _('Female'), click=lambda screen: screen.sim.update('gender', False),
                                     key='genderFemale', radio='gender', state=gui.STATE_ACTIVE if not self.sim.gender else gui.STATE_DEFAULT)
        self.buttons.append(btnGenderFemale)
        self.buttons.append(btnGenderMale)

        self.textAge = game.font.render(_('Age:'), False, colors.WHITE)
        self.ageInput = gui.SliderInput(pygame.Rect(175 + self.textAge.get_width(), 45, 250, 40), 6, 100, self.sim.age,
                                        key='age', onChange=lambda value: game.currentScreen.sim.update('age', value))
        self.inputs.append(self.ageInput)

        btnAddSim = gui.Button(pygame.Rect(self.screenWidth-buttonWidth*2-26, self.screenHeight-50, buttonWidth, 40), _('Confirm'), key='addSim')
        self.buttons.append(btnAddSim)
        btn = gui.Button(pygame.Rect(self.screenWidth-buttonWidth-16, self.screenHeight-50, buttonWidth, buttonHeight), _('Back'),
                         lambda self: game.changeScreen('CreateFamily', {'world': self.config['world']}), icon=images.BUTTON_BACK, key=pygame.K_ESCAPE)
        self.buttons.append(btn)

    def clear(self):
        game.surface.fill(colors.LIGHT_BLUE)
        super().clear()

    def reset(self):
        super().reset()
        self.state = 'default'

    def draw(self):
        # pygame.draw.rect(game.surface, colors.WHITE, (5, 5, 625, 135), 2)
        game.surface.blit(self.textFirstName, (10, 15))
        game.surface.blit(self.textLastName, (self.inputFirstName.rect.right + 10, 15))
        game.surface.blit(self.textAge, (170, 55))

        body = 'male' if self.sim.gender else 'female'
        if self.sim.gender and self.sim.age < 16:
            body = 'kid'
        elif not self.sim.gender and self.sim.age < 13:
            body = 'kid'
        game.surface.blit(images.SPRITES_CAS_BODY[body], (30, 100))

        if self.sim.age < 5:
            head = 'baby'
        elif self.sim.gender:
            if self.sim.age < 12:
                head = 'male-kid'
            elif self.sim.age < 18:
                head = 'male-teen'
            elif self.sim.age < 30:
                head = 'male-young'
            else:
                head = 'male-old'
        else:
            if self.sim.age < 9:
                head = 'female-kid'
            elif self.sim.age < 12:
                head = 'female-kid2'
            elif self.sim.age < 16:
                head = 'female-teen'
            elif self.sim.age < 22:
                head = 'female-young'
            elif self.sim.age < 30:
                head = 'female-old'
            else:
                head = 'female-old2'

        game.surface.blit(images.SPRITES_CAS_HEAD[head], (30, 70))
        super().draw()


