import pygame
import colors
import gui
import game
import images
from translate import _
import savefile
from gamescreen import GameScreen


class SelectWorld(GameScreen):
    def __init__(self):
        super(SelectWorld, self).__init__()
        self.worlds = []
        self.reset()
        self.offset = 5

        buttonWidth = 120
        buttonHeight = 40
        y = self.offset*6
        for world in self.worlds:
            btn = gui.Button(pygame.Rect(self.screenWidth-buttonWidth-self.offset*6, y, buttonWidth, buttonHeight), _('Load'),
                             lambda self: game.changeScreen('WorldPreview', {'world':world}), key=world, state=gui.STATE_DEFAULT if world.isValid else gui.STATE_DISABLED)
            self.buttons.append(btn)
            y += 50+self.offset*2

        btn = gui.Button(pygame.Rect(self.screenWidth/2-buttonWidth/2, self.screenHeight-100, buttonWidth, buttonHeight), _('Back'),
                         lambda self: game.changeScreen('MainMenu'), icon=images.BUTTON_BACK, key=pygame.K_ESCAPE)
        self.buttons.append(btn)

    def reset(self):
        super().reset()
        self.worlds = savefile.getAllWorlds()

    def draw(self):
        self.drawBackground()
        y = self.offset*5

        for world in self.worlds:
            rect = (self.offset * 5, y, self.screenWidth - self.offset * 10, 50)
            pygame.draw.rect(game.surface, colors.WHITE, rect, 2)
            color = colors.WHITE if world.isValid else colors.GRAY
            render = game.font.render(world.name + ' (version: '+world.version+')', False, color)
            game.surface.blit(render, (self.offset * 7, y + 25 - render.get_height() / 2))
            y += 50+self.offset*2

        super().draw()

    def clear(self):
        game.surface.fill(colors.LIGHT_BLUE)
        super().clear()

    def drawBackground(self):
        render = game.font.render(_("Select world"), False, colors.WHITE, colors.LIGHT_BLUE)
        game.surface.blit(render, (self.screenWidth / 2 - render.get_width() / 2, 1))
