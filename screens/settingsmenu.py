import pygame
import colors
import gui
import game
import images
import settings
from translate import _
from gamescreen import GameScreen


class SettingsMenu(GameScreen):

    def __init__(self):
        super(SettingsMenu, self).__init__()
        self.offset = 5
        buttonWidth = 130
        buttonHeight = 40

        languages = (
            ("en", "English", lambda self: self.selectLanguage('en'), images.FLAG_USA),
            ("ru", "Русский", lambda self: self.selectLanguage('ru'), images.FLAG_RUSSIA),
        )
        x = 200
        for key, name, click, icon in languages:
            active = key == game.settings["language"]
            btn = gui.Button(pygame.Rect(x, self.offset*7+50, buttonWidth, buttonHeight), name, click,
                             state=gui.STATE_ACTIVE if active else gui.STATE_DEFAULT, radio='language', icon=icon, key=key)
            self.buttons.append(btn)
            x += buttonWidth+5

        btn = gui.Button(pygame.Rect(self.screenWidth/2-buttonWidth/2, self.screenHeight-100, buttonWidth, buttonHeight), _('Back'),
                         lambda self: game.changeScreen('MainMenu'), icon=images.BUTTON_BACK, key=pygame.K_ESCAPE)
        self.buttons.append(btn)

    def draw(self):
        self.drawBackground()
        super().draw()

    def drawBackground(self):

        render = game.font.render(_("Settings"), False, colors.WHITE, colors.LIGHT_BLUE)
        game.surface.blit(render, (self.screenWidth/2-render.get_width()/2, 1))

        rect = (self.offset*5, self.offset*5, self.screenWidth-self.offset*10, 50)
        pygame.draw.rect(game.surface, colors.WHITE, rect, 2)
        render = game.font.render(_("Some shit"), False, colors.WHITE)
        game.surface.blit(render, (self.offset*7, self.offset*5+25-render.get_height()/2))

        rect = (self.offset*5, self.offset*6+50, self.screenWidth-self.offset*10, 50)
        pygame.draw.rect(game.surface, colors.WHITE, rect, 2)
        render = game.font.render(_("Language"), False, colors.WHITE)
        game.surface.blit(render, (self.offset*7, self.offset*6+75-render.get_height()/2))

    def clear(self):
        game.surface.fill(colors.LIGHT_BLUE)
        super().clear()

    def selectLanguage(self, code):
        game.settings["language"] = code
        settings.save(game.settings)

