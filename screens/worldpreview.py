import pygame
import colors
import gui
import game
import images
from translate import _
import units
import sprites
import random
import utils
from gamescreen import GameScreen


class WorldPreview(GameScreen):
    def __init__(self):
        super().__init__()
        self.reset()
        self.city = []
        for j in range(0, 16):
            row = []
            for i in range(0, 16):
                r = random.random()
                if r < 0.05:
                    c = ' '
                elif (i % 3 == 0 or j % 3 == 0) and r < 0.99:
                    c = '%'
                elif 3 < j < 13 and 3 < i < 13 and r < 0.7:
                    c = '@'
                elif r < 0.9:
                    c = 'h'
                else:
                    c = 's'
                row.append(c)
            self.city.append(row)

        cell = 32
        offsetX = 100
        offsetY = 100
        roads = sprites.ConnectedSpritesheet('res/roads.png', None, None, 32)
        buildings = sprites.spritesheet('res/buildings.png')
        j = 0
        for row in self.city:
            y = j * cell + offsetY
            i = 0
            for c in row:
                x = i * cell + offsetX
                if c == '%':
                    unit = units.ConnectedUnit(roads.map, (x, y), None, zIndex=-1)
                    right = (i - 1, j) if i - 1 >= 0 else False
                    left = (i + 1, j) if i + 1 <= len(row) - 1 else False
                    top = (i, j - 1) if j - 1 >= 0 else False
                    bottom = (i, j + 1) if j + 1 <= len(self.city) - 1 else False

                    unit.connect((left and self.city[left[1]][left[0]] == '%',
                                  right and self.city[right[1]][right[0]] == '%',
                                  top and self.city[top[1]][top[0]] == '%',
                                  bottom and self.city[bottom[1]][bottom[0]] == '%'))
                    self.units.append(unit)
                elif c == 'h':
                    unit = units.Unit(buildings.image_at((0, 0, 32, 32)), (x, y), None, zIndex=0, hoverable=True)
                    self.units.append(unit)
                elif c == 's':
                    unit = units.Unit(buildings.image_at((32, 0, 32, 32)), (x, y), None, zIndex=0)
                    self.units.append(unit)
                elif c == 'f':
                    unit = units.Unit(buildings.image_at((64, 0, 32, 32)), (x, y), None, zIndex=0)
                    self.units.append(unit)
                elif c == 'o':
                    unit = units.Unit(buildings.image_at((96, 0, 32, 32)), (x, y), None, zIndex=0)
                    self.units.append(unit)
                elif c == 'l':
                    unit = units.Unit(buildings.image_at((0, 32, 32, 32)), (x, y), None, zIndex=0)
                    self.units.append(unit)
                elif c == 'b':
                    unit = units.Unit(buildings.image_at((32, 32, 32, 32)), (x, y), None, zIndex=0)
                    self.units.append(unit)
                elif c == 'c':
                    unit = units.Unit(buildings.image_at((64, 32, 32, 32)), (x, y), None, zIndex=0)
                    self.units.append(unit)
                elif c == 't':
                    unit = units.Unit(buildings.image_at((96, 32, 32, 32)), (x, y), None, zIndex=0)
                    self.units.append(unit)
                elif c == '@':
                    unit = units.Unit(buildings.image_at((32, 64, 32, 32)), (x, y), None, zIndex=0, hoverable=True)
                    self.units.append(unit)
                i += 1
            j += 1

        buttonWidth = 120
        buttonHeight = 40
        btn = gui.Button(pygame.Rect(self.screenWidth-buttonWidth-16, 16, buttonWidth, buttonHeight), _('Menu'),
                         lambda self: game.changeScreen('MainMenu'), icon=images.BUTTON_BACK, key=pygame.K_ESCAPE)
        self.buttons.append(btn)
        btn = gui.Button(pygame.Rect(self.screenWidth-buttonWidth-16, buttonHeight+21, buttonWidth, buttonHeight), _('New family'),
                         lambda self: game.changeScreen('CreateFamily', {'world': self.config['world']}, True), icon=None, key=pygame.K_n)
        self.buttons.append(btn)

    def reset(self):
        super().reset()
        game.surface = pygame.Surface((1920, 1280))
        game.surface = game.surface

    def clear(self):
        game.surface.fill(colors.GREEN)
        super().clear()

    def call(self, event):
        super().call(event)

        if event.type == pygame.MOUSEBUTTONDOWN and event.button == 5:
            game.camera.zoomOut()
        elif event.type == pygame.MOUSEBUTTONDOWN and event.button == 4:
            game.camera.zoomIn()
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_w:
                self.moveCamera += gui.CAMERA_MOVE_UP
            if event.key == pygame.K_a:
                self.moveCamera += gui.CAMERA_MOVE_LEFT
            if event.key == pygame.K_s:
                self.moveCamera += gui.CAMERA_MOVE_DOWN
            if event.key == pygame.K_d:
                self.moveCamera += gui.CAMERA_MOVE_RIGHT
        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_w:
                self.moveCamera -= gui.CAMERA_MOVE_UP
            if event.key == pygame.K_a:
                self.moveCamera -= gui.CAMERA_MOVE_LEFT
            if event.key == pygame.K_s:
                self.moveCamera -= gui.CAMERA_MOVE_DOWN
            if event.key == pygame.K_d:
                self.moveCamera -= gui.CAMERA_MOVE_RIGHT



    def draw(self):
        super().draw()



