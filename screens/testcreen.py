import pygame
import colors
import gui
import game
import images
from translate import _
import units
import sprites
import utils
from gamescreen import GameScreen

class TestScreen(GameScreen):
    def __init__(self):
        super().__init__()
        self.reset()
        self.player = units.AutoOrientedUnit(images.SPRITES_TEST_ALICE, (200, 300))
        self.units.append(self.player)

        room = [
            '##############',
            '#........#...#',
            '#........#...#',
            '#............#',
            '#######.######',
            '   #.........#',
            '   #......####',
            '   #......#   ',
            '   ####.###   ',
        ]

        cell = 80
        sprite = sprites.ConnectedSpritesheet('res/wall.png', colors.YELLOW, None, 80)
        spriteBlock = sprites.ConnectedSpritesheet('res/wall-blocked.png', None, None, 80)

        offsetX = 40
        offsetY = 80
        j = 0
        for row in room:
            i = 0
            y = offsetX+j*cell
            for c in row:
                x = offsetY+i*cell
                if c == '#':
                    unit = units.ConnectedUnit(sprite.map, (x, y), None, zIndex=y+cell/2-5, blockedMap=spriteBlock.map)
                    right = (i-1, j) if i-1 >= 0 else False
                    left = (i+1, j) if i+1 <= len(row)-1 else False
                    top = (i, j-1) if j-1 >= 0 else False
                    bottom = (i, j+1) if j+1 <= len(room)-1 else False

                    unit.connect((left and room[left[1]][left[0]] == '#',
                                  right and room[right[1]][right[0]] == '#',
                                  top and room[top[1]][top[0]] == '#',
                                  bottom and room[bottom[1]][bottom[0]] == '#'))
                    self.units.append(unit)
                    self.blockableUnits.append(unit)
                i += 1
            j += 1

        buttonWidth = 120
        buttonHeight = 40
        btn = gui.Button(pygame.Rect(32, 32, buttonWidth, buttonHeight), _('Back'),
                         lambda self: game.changeScreen('MainMenu'), icon=images.BUTTON_BACK, key=pygame.K_ESCAPE)
        self.buttons.append(btn)



    def reset(self):
        super().reset()
        game.surface = pygame.Surface((2000, 2000))
        game.surface = game.surface

    def clear(self):
        game.surface.fill(colors.GREEN)
        super().clear()

    def call(self, event):
        super().call(event)
        if event.type == pygame.MOUSEBUTTONDOWN and event.button == 3:
            self.player.target = utils.displayPointToSurface(event.pos)
        elif event.type == pygame.MOUSEBUTTONDOWN and event.button == 5:
            game.camera.zoomOut()
        elif event.type == pygame.MOUSEBUTTONDOWN and event.button == 4:
            game.camera.zoomIn()
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_w:
                self.moveCamera += gui.CAMERA_MOVE_UP
            if event.key == pygame.K_a:
                self.moveCamera += gui.CAMERA_MOVE_LEFT
            if event.key == pygame.K_s:
                self.moveCamera += gui.CAMERA_MOVE_DOWN
            if event.key == pygame.K_d:
                self.moveCamera += gui.CAMERA_MOVE_RIGHT
        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_w:
                self.moveCamera -= gui.CAMERA_MOVE_UP
            if event.key == pygame.K_a:
                self.moveCamera -= gui.CAMERA_MOVE_LEFT
            if event.key == pygame.K_s:
                self.moveCamera -= gui.CAMERA_MOVE_DOWN
            if event.key == pygame.K_d:
                self.moveCamera -= gui.CAMERA_MOVE_RIGHT



    def drawBg(self):
        pass

    def draw(self):
        self.drawBg()
        super().draw()

