import pygame
import settings as Settings
import colors
import gamescreen
import time
import images
import translate
import gui

WINDOW_TITLE = 'MySims'
FPS = 60
CAMERA_SPEED = 10

settings = None
display = None
surface = None
staticSurface = None
currentScreen = None
screensCache = dict()
font = None
camera = gui.Camera((0, 0), 1)
clock = None

def init():
    '''
     Game initialisation
    '''

    global settings, display, surface, staticSurface, font, camera, clock

    settings = Settings.load()
    pygame.init()
    pygame.font.init()
    pygame.display.set_caption(WINDOW_TITLE)
    pygame.display.set_icon(images.WINDOW_ICON)
    font = pygame.font.SysFont('Ubuntu', 18, bold=True)
    translate.init(settings["language"])

    display = pygame.display.set_mode((settings["resolution"]["width"], settings["resolution"]["height"]), pygame.HWSURFACE | pygame.DOUBLEBUF | pygame.RESIZABLE)
    surface = pygame.Surface((display.get_width(), display.get_height()))
    staticSurface = pygame.Surface((display.get_width(), display.get_height()))
    camera.move((display.get_width()/2, display.get_height()/2))
    images.load()
    clock = pygame.time.Clock()


def loop():
    '''
     Main loop
    '''
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                exit(0)
            elif event.type == pygame.VIDEORESIZE:
                pygame.display.set_mode(event.dict['size'], pygame.HWSURFACE | pygame.DOUBLEBUF | pygame.RESIZABLE)
                screensCache.clear()
                changeScreen(currentScreen.__class__.__name__, currentScreen.config, True)
            else:
                currentScreen.call(event)

        currentScreen.tick()
        flush()
        clock.tick(FPS)


def cameraZoom(zoom):
    z = 1
    for i in range(0, zoom-1):
        z *= 0.8
    return z

def flush():
    # print(camera.zoom)
    sss = pygame.transform.scale(surface, (int(surface.get_width()*cameraZoom(camera.zoom)), int(surface.get_height()*cameraZoom(camera.zoom))))
    display.blit(sss, (-camera.getBounds().left, -camera.getBounds().top))
    display.blit(staticSurface, (0, 0))
    pygame.display.update()


def changeScreen(name, config=None, forceReset=False):
    '''

    '''
    global currentScreen, surface, staticSurface

    if currentScreen:
        currentScreen.mode = gamescreen.MODE_DISABLED

    surface = pygame.Surface((display.get_width(), display.get_height()))
    surface.fill(colors.BLACK)
    staticSurface = pygame.Surface((display.get_width(), display.get_height()), pygame.SRCALPHA, 32).convert_alpha()

    camera.pos = (display.get_width()/2, display.get_height()/2)
    camera.zoom = 1

    if name in screensCache:
        currentScreen = screensCache[name]
        if forceReset:
            currentScreen.reset()
    else:
        currentScreen = getattr(gamescreen, name)()
        screensCache[name] = currentScreen

    currentScreen.load(config)
    currentScreen.mode = gamescreen.MODE_ACTIVE
    currentScreen.clear()
    currentScreen.draw()
    flush()

