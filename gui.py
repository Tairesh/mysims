import pygame
import colors
import numpy
import game
import time

STATE_DEFAULT = 0
STATE_HOVER = 1
STATE_ACTIVE = 2
STATE_DISABLED = -1

CAMERA_MOVE_UP = 1
CAMERA_MOVE_DOWN = 2
CAMERA_MOVE_LEFT = 4
CAMERA_MOVE_RIGHT = 8


class Camera:
    def __init__(self, pos, zoom = 1):
        self.pos = pos
        self.zoom = zoom

    def move(self, delta):
        pos = (self.pos[0] + delta[0], self.pos[1] + delta[1])
        if game.display.get_width()/2 <= pos[0] < game.surface.get_width()-game.display.get_width()/2 and game.display.get_height()/2 <= pos[1] < game.surface.get_height() - game.display.get_height()/2:
            self.pos = pos

    def zoomOut(self):
        if self.zoom < 20:
            self.zoom += 1

    def zoomIn(self):
        if self.zoom > 1:
            self.zoom -= 1

    def getBounds(self):
        return pygame.Rect((self.pos[0]-game.display.get_width()/2, self.pos[1]-game.display.get_height()/2, game.display.get_width(), game.display.get_height()))


class BaseInput(pygame.sprite.DirtySprite):
    def __init__(self, key=None, state=STATE_DEFAULT, groups=()):
        self.state = state
        self.key = key
        super().__init__(*groups)

    def hover_on(self):
        if self.state == STATE_DISABLED:
            return
        if self.state == STATE_DEFAULT:
            self.state = STATE_HOVER

    def hover_off(self):
        if self.state == STATE_DISABLED:
            return
        if self.state == STATE_HOVER:
            self.state = STATE_DEFAULT

    def active_on(self):
        if self.state == STATE_DISABLED:
            return
        self.state = STATE_ACTIVE

    def active_off(self):
        if self.state == STATE_DISABLED:
            return
        self.state = STATE_DEFAULT


class BaseButton(BaseInput):
    def __init__(self, click=lambda screen: None, state=STATE_DEFAULT, radio=None, key=None, groups=()):
        self.click = click
        self.radio = radio
        super().__init__(key, state, groups)


class ImageButton(BaseButton):
    def __init__(self, pos, image, image_hover, image_active,
                 click=lambda self: None, state=STATE_DEFAULT, radio=None, key=None, groups=()):
        self.pos = pos
        self.image = image
        self.image_hover = image_hover
        self.image_active = image_active
        self.rect = pygame.Rect(pos[0], pos[1], image.get_width(), image.get_height())
        super().__init__(click, state, radio, key, groups)

    def draw(self, surface):
        if self.state == STATE_HOVER:
            self.image = self.image_hover
        if self.state == STATE_ACTIVE:
            self.image = self.image_active
        super().draw(surface)


class Button(BaseButton):

    def __init__(self, rect, text, click = lambda self: None,
                 mainColor=colors.WHITE, hoverColor=colors.DARK_BLUE, activeColor=colors.BROWN, backgroundColor=colors.LIGHT_BLUE,
                 state=STATE_DEFAULT, radio=None, icon=None, key=None):
        super().__init__(click, state, radio, key)
        self.rect = rect
        self.text = text
        self.mainColor = mainColor
        self.backgroundColor = backgroundColor
        self.hoverColor = hoverColor
        self.activeColor = activeColor
        self.icon = icon

    def draw(self):

        color = self.mainColor
        if self.state == STATE_HOVER:
            color = self.hoverColor
        if self.state == STATE_ACTIVE:
            color = self.activeColor

        pygame.draw.rect(game.staticSurface, self.backgroundColor, self.rect)
        pygame.draw.rect(game.staticSurface, color, self.rect, 2)
        render = game.font.render(self.text, False, color)

        if self.icon:
            posIcon = numpy.array(self.rect.topleft) + numpy.array((10, 5))
            game.staticSurface.blit(self.icon, posIcon)
            posText = (posIcon[0]+self.icon.get_width()+5, self.rect.center[1]-render.get_height()/2)
        else:
            posText = tuple(numpy.subtract(self.rect.center, (render.get_width()/2, render.get_height()/2)))

        game.staticSurface.blit(render, posText)


class SliderInput(BaseInput):
    def __init__(self, rect: pygame.Rect, start, end, value, key=None, state=STATE_DEFAULT,onChange=lambda value: None):
        super().__init__(key, state)
        self.rect = rect
        self.start = start
        self.end = end
        self.value = value
        self.step = (end-start)/(rect.width-45) # values per pixel
        self.color = colors.WHITE
        self.colorBackground = colors.LIGHT_BLUE
        self.colorLine = colors.WHITE
        self.colorLineActive = colors.DARK_BLUE
        self.lineLeft = self.rect.left + 40
        self.lineRight = self.rect.right - 5
        self.onChange = onChange

    def draw(self):
        value_surface = game.font.render(str(self.value), True, self.color)
        pygame.draw.rect(game.staticSurface, self.colorBackground, self.rect)
        game.staticSurface.blit(value_surface, (self.rect.left+5, self.rect.center[1]-value_surface.get_height()/2))

        color = self.colorLineActive if self.state == STATE_ACTIVE else self.colorLine
        line = (self.lineLeft, self.rect.center[1]-2, self.lineRight-self.lineLeft, 4)
        pygame.draw.rect(game.staticSurface, color, line)

        pos = (self.value-self.start)/self.step
        pusher = (self.lineLeft+pos-3, self.rect.top, 6, self.rect.height)
        pygame.draw.rect(game.staticSurface, color, pusher)

    def handle_event(self, event):
        if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
            if self.rect.collidepoint(event.pos):
                self.state = STATE_ACTIVE
                self._updateValue(event.pos[0])
            else:
                self.state = STATE_DEFAULT
        elif self.state == STATE_ACTIVE and event.type == pygame.MOUSEBUTTONUP and event.button == 1:
            self.state = STATE_DEFAULT
        elif self.state == STATE_ACTIVE and event.type == pygame.MOUSEMOTION:
            self._updateValue(event.pos[0])

    def _updateValue(self, x):

        if self.lineLeft <= x <= self.lineRight:
            self.value = self.start+round(self.step * (x - self.lineLeft))
        elif x < self.lineLeft:
            self.value = self.start
        elif x > self.lineRight:
            self.value = self.end
        self.onChange(self.value)

    def tick(self):
        return True


BLINK_PERIOD = 0.5


class TextInput(BaseInput):

    def __init__(self, x, y, w, h, text='', key=None, state=STATE_DEFAULT, onChange=lambda value: None):
        super().__init__(key, state)
        self.rect = pygame.Rect(x, y, w, h)
        self.color = colors.WHITE
        self.backgroundColor = colors.LIGHT_BLUE
        self.text = text
        self.txt_surface = game.font.render(text, True, self.color)
        self.blinkTimeout = BLINK_PERIOD
        self.blinkState = True
        self.blinkLast = 0
        self.onChange = onChange

    def handle_event(self, event):
        if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
            if self.rect.collidepoint(event.pos):
                self.state = STATE_ACTIVE
            else:
                self.state = STATE_DEFAULT
            # Change the current color of the input box.
            self.color = colors.DARK_BLUE if self.state == STATE_ACTIVE else colors.WHITE
        elif event.type == pygame.KEYDOWN:
            if self.state == STATE_ACTIVE:
                # if event.key == pygame.K_RETURN:
                #     print(self.text)
                #     self.text = ''
                if event.key == pygame.K_BACKSPACE:
                    self.text = self.text[:-1]
                    self.onChange(self.text)
                elif event.key != pygame.K_ESCAPE:
                    self.text += event.unicode
                    self.onChange(self.text)
                return True

        return False

    def draw(self):
        self.txt_surface = game.font.render(self.text, True, self.color)
        # Blit the rect.
        pygame.draw.rect(game.staticSurface, self.backgroundColor, self.rect)
        pygame.draw.rect(game.staticSurface, self.color, self.rect, 2)
        # Blit the text.
        game.staticSurface.blit(self.txt_surface, (self.rect.x+5, self.rect.y+5))

    def tick(self):
        needFlush = False
        now = time.time()
        if self.blinkTimeout > 0:
            self.blinkTimeout -= now - self.blinkLast
        else:
            self.blinkState = not self.blinkState
            self.blinkTimeout = BLINK_PERIOD

            cursor = game.font.render(' ', True, colors.WHITE, colors.WHITE if self.blinkState else colors.LIGHT_BLUE)
            game.staticSurface.blit(cursor, (self.rect.x+self.txt_surface.get_width()+7, self.rect.y+5))
            needFlush = True
        self.blinkLast = time.time()
        return needFlush
