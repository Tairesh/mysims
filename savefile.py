import os
import sqlite3
import utils
from translate import _

VERSION = '0.0.1'
DB_VERSION = 1


def worldnameToFilename(name):
    return os.path.join('save', name + '.save')


def listFiles():
    return [os.path.join('save', f) for f in os.listdir('save') if os.path.isfile(os.path.join('save', f)) and f.endswith('.save')]


def createNewFile(worldName):
    filename = worldnameToFilename(worldName)
    files = listFiles()
    if filename in files:
        print('File ' + filename + ' allready exists, aborting...')
        return False

    connection = sqlite3.connect(filename)
    cursor = connection.cursor()
    cursor.execute('PRAGMA journal_mode=WAL')
    cursor.execute('''
        CREATE TABLE system (
            name        TEXT UNIQUE NOT NULL,
            value        TEXT DEFAULT NULL
        );
            ''')

    cursor.execute('INSERT INTO system (name, value) VALUES (\'name\', \'' + worldName + '\');')
    cursor.execute('INSERT INTO system (name, value) VALUES (\'version\', \'' + VERSION + '\');')
    cursor.execute('INSERT INTO system (name, value) VALUES (\'dbVersion\', \'' + str(DB_VERSION) + '\');')
    cursor.execute('INSERT INTO system (name, value) VALUES (\'time\', \'0\');')

    cursor.execute('''
        CREATE TABLE sims (
            id          INTEGER
        )
    ''')

    connection.commit()
    connection.close()
    return True


class World:

    INT_FIELDS = {'dbVersion', 'time'}

    def __init__(self, savefile):
        self.connection = sqlite3.connect(savefile)
        cursor = self.connection.cursor()
        for name, value in cursor.execute('SELECT name, value FROM system'):
            if name in self.INT_FIELDS:
                value = int(value)
            setattr(self, name, value)
        self.connection.close()

    @property
    def displayedTime(self):
        seconds = self.time // 30
        minutes = seconds // 60
        hours = minutes // 60
        days = minutes // 24
        hours -= days * 24
        minutes -= days * 24 * 60 + hours * 60
        seconds -= days * 24 * 60 * 60 + hours * 60 * 60 + minutes * 60
        return _('Day') + ' ' + str(days + 1) + ". " + utils.intToZeroStarted(hours) + ":" + utils.intToZeroStarted(minutes) + ":" + utils.intToZeroStarted(seconds)

    def tick(self):
        self.time += 1

    @property
    def isValid(self):
        return self.dbVersion == DB_VERSION


def getAllWorlds():
    files = listFiles()
    worlds = []
    for filename in files:
        world = World(filename)
        worlds.append(world)

    return worlds