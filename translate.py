import os

LANG = 'en'
lines = dict()


def init(lang):
    global LANG
    LANG = lang
    if LANG != 'en':
        filename = os.path.join('res', 'locale', LANG+'.txt')
        with open(filename, 'r') as file:
            for line in file:
                key, value = line.split(' | ')
                lines[key] = value[:-1:]


def _(sentence):
    if LANG == 'en' or sentence not in lines:
        return sentence
    else:
        return lines[sentence]