import pygame
import sprites
import colors

WINDOW_ICON = pygame.image.load('res/icon.png')
LOGO = pygame.image.load('res/logo.png')

FLAG_RUSSIA = pygame.image.load('res/russia.png')
FLAG_USA = pygame.image.load('res/usa.png')

BUTTON_BACK = pygame.image.load('res/arrow-back-icon.png')
TARGET = pygame.image.load('res/target.png')

SPRITES_TEST_ALICE = None

SPRITES_ICONS = None
SPRITES_CAS_BODY = None
SPRITES_CAS_HEAD = None

def load():
    global SPRITES_TEST_ALICE, SPRITES_CAS_BODY, SPRITES_CAS_HEAD, SPRITES_ICONS

    sheet = sprites.spritesheet('res/unit.png')
    SPRITES_TEST_ALICE = {
        'north': sheet.image_at((0, 64, 64, 64)),
        'south': sheet.image_at((0, 0, 64, 64)),
        'west': sheet.image_at((64, 0, 64, 64)),
        'east': sheet.image_at((64, 64, 64, 64)),
    }

    sheet = sprites.spritesheet('res/cas.png')
    SPRITES_CAS_BODY = {
        'female': sheet.image_at((0, 0, 128, 128)),
        'male': sheet.image_at((128, 0, 128, 128)),
        'kid': sheet.image_at((256, 0, 128, 128)),
    }
    SPRITES_CAS_HEAD = {
        'male-kid': sheet.image_at((0, 128, 128, 128)),
        'male-teen': sheet.image_at((128, 128, 128, 128)),
        'male-young': sheet.image_at((256, 128, 128, 128)),
        'male-old': sheet.image_at((384, 128, 128, 128)),

        'baby': sheet.image_at((0, 256, 128, 128)),
        'female-kid': sheet.image_at((128, 256, 128, 128)),
        'female-kid2': sheet.image_at((256, 256, 128, 128)),
        'female-teen': sheet.image_at((384, 256, 128, 128)),

        'female-young': sheet.image_at((0, 384, 128, 128)),
        'female-old': sheet.image_at((128, 384, 128, 128)),
        'female-old2': sheet.image_at((256, 384, 128, 128)),
    }
